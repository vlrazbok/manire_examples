import {Component} from "@angular/core";
import {FormControl, FormGroup} from "@angular/forms";
import {MatDialogRef} from "@angular/material/dialog";
import {PeriodicElement} from "../../app.component";

@Component({
  templateUrl: 'create-modal.component.html',
  styleUrls: ['create-modal.component.scss']
})
export class CreateModalComponent {

  newItem: FormGroup;

  constructor(
    private dialogRef: MatDialogRef<CreateModalComponent>
  ) {
    this.newItem = new FormGroup<any>({
      position: new FormControl<number>(0),
      name: new FormControl<string>(''),
      weight: new FormControl<number>(0),
      symbol: new FormControl<string>(''),
    })
  }

  createItem() {
    // console.log(this.newItem.value)
    this.dialogRef.close(this.newItem.getRawValue())
  }
}
