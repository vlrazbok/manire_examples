import {Component, Inject} from "@angular/core";
import {FormControl, FormGroup} from "@angular/forms";
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";
import {PeriodicElement} from "../../app.component";

//Обов'язковий декоратор для компонентів, в якому прописуються посилання на шаблони, стилі, анімація
@Component({
    templateUrl: 'edit-modal.component.html',
    styleUrls: ['edit-modal.component.scss']
})
export class EditModalComponent {
  inputItem!: FormGroup;

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: PeriodicElement,
    private dialogRef: MatDialogRef<EditModalComponent>

  ) {
    this.inputItem = new FormGroup<any>({
      position: new FormControl<number>({
        value: 0,
        disabled: true
      }),
      name: new FormControl<string>(''),
      weight: new FormControl<number>(0),
      symbol: new FormControl<string>(''),
    })

    this.inputItem.patchValue(data)
  }

  updateItem() {
    this.dialogRef.close(this.inputItem.getRawValue())
  }
}


