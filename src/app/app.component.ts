import {ChangeDetectorRef, Component} from '@angular/core';
import {MatDialog} from "@angular/material/dialog";
import {CreateModalComponent} from "./modals/create/create-modal.component";
import {MatTableDataSource} from "@angular/material/table";
import {EditModalComponent} from "./modals/edit-modal/edit-modal.component";
export interface PeriodicElement {
  name: string;
  position: number;
  weight: number;
  symbol: string;
}

const ELEMENT_DATA: PeriodicElement[] = [
  {position: 1, name: 'Hydrogen', weight: 1.0079, symbol: 'H'},
  {position: 2, name: 'Helium', weight: 4.0026, symbol: 'He'},
  {position: 3, name: 'Lithium', weight: 6.941, symbol: 'Li'},
  {position: 4, name: 'Beryllium', weight: 9.0122, symbol: 'Be'},
  {position: 5, name: 'Boron', weight: 10.811, symbol: 'B'},
  {position: 6, name: 'Carbon', weight: 12.0107, symbol: 'C'},
  {position: 7, name: 'Nitrogen', weight: 14.0067, symbol: 'N'},
  {position: 8, name: 'Oxygen', weight: 15.9994, symbol: 'O'},
  {position: 9, name: 'Fluorine', weight: 18.9984, symbol: 'F'},
  {position: 10, name: 'Neon', weight: 20.1797, symbol: 'Ne'},
];
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  displayedColumns: string[] = ['position', 'name', 'weight', 'symbol', 'delete-action', 'edit-action'];
  dataSource: MatTableDataSource<PeriodicElement> = new MatTableDataSource<PeriodicElement>(ELEMENT_DATA);

  constructor(
    private matDialog: MatDialog,
  ) {
  }

  removeItem (itemPosition: number) : void{
    this.dataSource.data = this.dataSource.data.filter(element=>element.position !== itemPosition)
  }

  createItem() {
    this.matDialog.open(CreateModalComponent, {
      // width: '500px',
      // height: '400px'
    })
      .afterClosed()
      .subscribe((newItem: PeriodicElement)=> {
        if (!newItem)return;
        this.dataSource.data = [...this.dataSource.data, newItem]

      })
  }

  editItem(element: PeriodicElement): void {
    console.log('element:', element);

    this.matDialog.open(EditModalComponent, {
      data: element
    })
      .afterClosed()
      .subscribe(formValue => {
        // const foundPosition = this.dataSource.data.findIndex(item => item.position === formValue.position)
        //
        // const updatedArray =  this.dataSource.data;
        // updatedArray[foundPosition] = formValue;
        //
        // this.dataSource.data = updatedArray;

        const updatedArray =  this.dataSource.data;

        updatedArray.forEach((element,index) => {
          if (element.position === formValue.position) updatedArray[index] = formValue
        })

        this.dataSource.data = updatedArray;

    })
  }
}
